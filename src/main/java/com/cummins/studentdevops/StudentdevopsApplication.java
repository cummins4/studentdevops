package com.cummins.studentdevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentdevopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentdevopsApplication.class, args);
	}

}
